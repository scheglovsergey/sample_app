require 'spec_helper'

describe "Static pages" do

  # Что это за хрень?
  subject { page }
  # Найти документацию по Rspec и узнать что есть shared_examples_for
  # ЯЩИТАЮ, что это шаблон для тестирования соответствующих значений. В данном
  # случае это проверка наличия заголовка с тегом <h1> с содержимы м типа текст
  # в heading и наличия тайтла c содержимым в page_title
  shared_examples_for "all static pages" do
    it { should have_selector('h1', text: heading) }
    it { should have_title(full_title(page_title)) }
  end
  # Обычный блок для описания теста
  describe "Home page" do
    # Указываем путь к странице, которую тестируем, в данном случае:
    # Test root page
    before { visit root_path }
    # Создание локальной переменной с инициализированным значением в скобках { }
    # Переменная создается в момент запроса к ней (т.е. при её использовании),
    # а не в момент назначения
    let(:heading) { 'Sample App' }
    let(:page_title) { '' }
    # Мы отмечаем, что две команды let должны быть отработаны с использованием
    # КАКЯЩИТАЮ шаблона под названием "all static pages" в противном случае
    # let выполнены не будут и тестрирование наличия закоголвка и тайтла,
    # согласно шаблону, не произойдёт
    it_should_behave_like "all static pages"
    it { should_not have_content('| Home') }

  end
  # Test Help page
  describe "Help page" do

    before {visit help_path}

    let(:heading) {'Help'}
    let(:page_title) {'Help'}

    it_should_behave_like "all static pages"
  end
  # Test About page
  describe "About page" do

    before { visit about_path }

    let(:heading) {'About Us'}
    let(:page_title) {'About Us'}

    it_should_behave_like "all static pages"
  end
  # Test Contact page
  describe "Contact page" do

    before { visit contact_path }

    let(:heading) {'Contact'}
    let(:page_title) {'Contact'}

    it_should_behave_like "all static pages"
  end

  it "should have the rigth links on the layout" do
    visit root_path
    click_link "About"
    expect(page).to have_title(full_title('About Us'))
    click_link "Help"
    expect(page).to have_title(full_title('Help'))
    click_link "Home"
    click_link "Sign up now!"
    expect(page).to have_title(full_title('Sign up'))
    click_link "sample app"
    expect(page).to have_title(full_title(''))
  end

end
